$(document).ready(function(){

  // ##############
  // #Send User ID Login
  // ##############

  $("#submitUserIDLogin").click(function(e){
    // var reason = theForm.userId.
    e.preventDefault();
    var userId = document.getElementById("userIdForm").value;

    if (userId != "") //if the input is not empty
    {
      document.getElementById("userId1").innerHTML = userId; //Write the userId into the password login form
      switchUI(loginDiv.id,userPasswordDiv.id);

      //Hide errors if the user come back on the form
      document.getElementById("ErrorUserEmpty_userIdForm").style = "display: none;"
      document.getElementById("userIdForm").style = "border-color : none;"
    }
    else //Show an error
    {
      document.getElementById("ErrorUserEmpty_userIdForm").style = "display: inline;"
      document.getElementById("userIdForm").style = "border-color : #EC0101;"
    }
    console.log(userId);
  });


  // ##############
  // #Send User password Login
  // ##############

  $("#submitUserPasswordLogin").click(function(e){
    // var reason = theForm.userId.
    e.preventDefault();
    var userPassword = document.getElementById("userPasswordForm").value;

    if (userPassword != "") //if the input is not empty
    {
      //   $.post(
      //     'http://localhost:3000/login',
      //     {
      //       // alert("Data: " + data + "\nStatus: " + status);
      //       userId : $("#userIdForm").val(), //Envoie du message de l'utilisateur
      //       userPassword : $("#userPasswordForm").val()
      //     },
      //
      //     'text'
      //    );
      //   console.log(userPassword);
      $.ajax({
        url: "http://127.0.0.1:3000/login",
        type: "POST",
        data: {userId: $("#userIdForm").val(), userPassword: $("#userPasswordForm").val()},
        cache: false,
        timeout: 5000,
        success: function (data) {
          var ret = JSON.parse(data);
          $('#ErrorFromServer_userPasswordForm').html(ret.msg);
          document.getElementById("ErrorFromServer_userPasswordForm").style = "display: inline;";
          document.getElementById("userPasswordForm").style = "border-color : #EC0101;";

          console.log("Success :" + ret.msg);
        },
        error: function (xhr, status, error) {
          console.log('Error: ' + error.message);
          $('#ErrorFromServer_userPasswordForm').html('Error connecting to the server.');
        },
      });
    }
    else //Show an error
    {
      $('#ErrorFromServer_userPasswordForm').html("User can't be empty !");
      document.getElementById("ErrorFromServer_userPasswordForm").style = "display: inline;"
      document.getElementById("userPasswordForm").style = "border-color : #EC0101;"
    }

  });


  // ##############
  // #Send User ID Reset
  // ##############

  $("#submitResetUserID").click(function(e){
    // var reason = theForm.userId.
    e.preventDefault();
    var userIdReset = document.getElementById("ResetUserID").value;

    if (userIdReset != "") //if the input is not empty
    {
      console.log(userIdReset);
    }
    else //Show an error
    {
      document.getElementById("ErrorUserEmpty_ResetUserID").style = "display: inline;"
      document.getElementById("ResetUserID").style = "border-color : #EC0101;"
    }
  });
});
