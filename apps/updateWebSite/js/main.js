//Get WebSite configurations
// Exemple of returned value :
//   -name
//   -directory
var webSiteInfo = "";
$.ajax({
  url: "http://127.0.0.1:3000/getWebSiteInfo",
  type: "POST",
  cache: false,
  timeout: 5000,
  success: function (data) {
    webSiteInfo = JSON.parse(data);
    websiteListCreateElements();
  },
  error: function (xhr, status, error) {
    console.log('Error: ' + error.message);
  },
});

function websiteListCreateElements ()
{
  // The final structure looks like :
  // <tr id="tr000-default">
  //   <td id="td000-default">
  //     <label id="000-default" class="inline_input containerRadioInput">000-default
  //       <input id="0" name="website" type="radio">
  //       <span class="checkmark"></span>
  //     </label>
  //   </td>
  // </tr>

  for (var i = 0; i < webSiteInfo.length; i++) {

    var trElement = document.createElement("tr");
    trElement.id = "tr" + webSiteInfo[i];
    var inElement = document.getElementById("listWebsite");
    inElement.appendChild(trElement);

    var tdElement = document.createElement("td");
    tdElement.id = "td" + webSiteInfo[i];
    var inElement = document.getElementById("tr" + webSiteInfo[i]);
    inElement.appendChild(tdElement);

    var labeWebSite = document.createElement("label");
    labeWebSite.id = webSiteInfo[i];
    labeWebSite.innerHTML = webSiteInfo[i];
    labeWebSite.className = "inline_input containerRadioInput";
    var inElement = document.getElementById("td" + webSiteInfo[i]);
    inElement.appendChild(labeWebSite);

    var inputWebSite = document.createElement("INPUT");
    inputWebSite.id = i;
    var inElement = document.getElementById(webSiteInfo[i]);
    inElement.appendChild(inputWebSite);
    document.getElementById(i).name = "website";
    document.getElementById(i).type = "radio";

    var spaninputWebSite = document.createElement("span");
    spaninputWebSite.className = "checkmark";
    var inElement = document.getElementById(webSiteInfo[i]);
    inElement.appendChild(spaninputWebSite);
  }
}


function updateForm() {
  //Upadte the behaviour of form with the differents input

  //Update for the method of update : GitUrl or Zip file
  if (document.getElementById("updateGitRadio").checked == true)
  switchUI(divDropFileForUpdate.id,gitURL.id,false)
  else
  switchUI(gitURL.id,divDropFileForUpdate.id,false)
}

window.onload = setInterval(updateForm, 200); //Call the function every 200ms after the page is load.
