// #########################################
// Description : Creat the main aside in apps (Main aside is on the right, it allow to switch between the differents apps ans it show user information like : email, pseudo..)
// Parameters  : None
// Return      : None
// #########################################

//Get apps configurations
// Exemple of returned value :
//   -name
//   -link
var appsInfo = "";
$.ajax({
  url: "http://127.0.0.1:3000/getApps",
  type: "POST",
  cache: false,
  timeout: 5000,
  success: function (data) {
    appsInfo = JSON.parse(data);
    appsInfo = appsInfo.appsInfo;
    check();
  },
  error: function (xhr, status, error) {
    console.log('Error: ' + error.message);
  },
});

//Get user info, email and user ID
var userInfo = "";
$.ajax({
  url: "http://127.0.0.1:3000/userInfo",
  type: "POST",
  cache: false,
  timeout: 5000,
  success: function (data) {
    userInfo = JSON.parse(data);
    check();
  },
  error: function (xhr, status, error) {
    console.log('Error: ' + error.message);
  },
});

//if all post request have reveived their answers, call the function createElements
var i = 0;
function check()
{
  i++;
  if (i==2)
  createElements();
}

// ##############
// # CREATS ELEMENTS
// ##############
function createElements()
{
  //create the main aside
  var mainAside = document.createElement("aside");
  mainAside.id = "mainAside";
  document.body.appendChild(mainAside);

  //Create button to hide or show mainAside on phone
  // <button class="btn" onclick="switcViewhMainAside()" ><i class="fa fa-bars"></i></button>
  var buttonSwitchView = document.createElement("BUTTON");
  buttonSwitchView.classList.add("btnSwitchView");
  buttonSwitchView.id = "buttonSwitchView";

  buttonSwitchView.onclick = function switcViewhMainAside ()
  {
    if (document.getElementById('mainAside').style.display == "block")
    document.getElementById("mainAside").style.display = "none";
    else
    document.getElementById("mainAside").style.display = "block";
  };

  buttonSwitchView.innerHTML = "<i class=\"fa fa-bars\">";
  document.body.appendChild(buttonSwitchView);



  //create the div for Log out user
  var divLogOut = document.createElement("div");
  divLogOut.id = "divLogOut";
  var inElement = document.getElementById("mainAside");
  inElement.appendChild(divLogOut);

  //Create elements in the div log out.
  //create icone of users
  var divfaviconUser = document.createElement("div");
  divfaviconUser.id = "divfaviconUser";
  var inElement = document.getElementById("divLogOut");
  inElement.appendChild(divfaviconUser);

  var divfaviconUserLetters = document.createElement("p");
  divfaviconUserLetters.id = "divfaviconUserLetters";
  var inElement = document.getElementById("divfaviconUser");
  inElement.appendChild(divfaviconUserLetters);
  document.getElementById("divfaviconUserLetters").innerHTML = userInfo.user.slice(0, 2).toUpperCase();


  //create <p> to show user email
  var pUserEmail = document.createElement("p");
  pUserEmail.id = "pUserEmail";
  var inElement = document.getElementById("divLogOut");
  inElement.appendChild(pUserEmail);
  document.getElementById("pUserEmail").innerHTML = userInfo.userMail;

  //create log out button
  var logOut = document.createElement("a");
  var linkText = document.createTextNode("Log out");
  logOut.appendChild(linkText);
  logOut.classList.add("logOutButton");
  logOut.title = "log out";
  logOut.href = "http://127.0.0.1/apps/";
  //Put apps in the div for apps
  var inElement = document.getElementById("divLogOut");
  inElement.appendChild(logOut);



  //create the div for Apps
  var divApps = document.createElement("div");
  divApps.id = "divApps";
  divApps.className = "mainAside";
  var inElement = document.getElementById("mainAside");
  inElement.appendChild(divApps);

  //create the list of apps in div above
  for (var i = 0; i < appsInfo.length; i++) {
    var counter = appsInfo[i];
    console.log(counter.name);
    if(counter.name != "none")
    {
      var app = document.createElement("a");
      var linkText = document.createTextNode(counter.name);
      app.appendChild(linkText);
      app.classList.add("applist");
      app.title = counter.name;
      app.href = "http://127.0.0.1/apps/" + counter.id;
      //document.body.appendChild(app);

      //Put apps in the div for apps
      var inElement = document.getElementById("divApps");
      inElement.appendChild(app);
    }
  }

  //create the footer
  var footer = document.createElement("footer");
  footer.classList.add("footerMainAside");
  footer.id = "footerMainAside";
  //Put footer in the main aside
  var inElement = document.getElementById("mainAside");
  inElement.appendChild(footer);

  //create the spans for W.S.U
  var span = document.createElement("span");
  span.classList.add("W");
  span.id = "W";
  var inElement = document.getElementById("footerMainAside");
  inElement.appendChild(span);
  document.getElementById("W").innerHTML = "W";

  var span = document.createElement("span");
  span.classList.add("S");
  span.id = "S";
  var inElement = document.getElementById("footerMainAside");
  inElement.appendChild(span);
  document.getElementById("S").innerHTML = "S";

  var span = document.createElement("span");
  span.classList.add("U");
  span.id = "U";
  var inElement = document.getElementById("footerMainAside");
  inElement.appendChild(span);
  document.getElementById("U").innerHTML = "U";
}

setInterval(test, 500);
function test() {
  // Get the window dimensions
var $window = $(window);
var wWidth  = $window.width();
if (wWidth > 700)
document.getElementById('mainAside').style.display = "block";

}
