// #########################################
// Description : Hide a html element to show another. Exemple : Switch between user input ans password imput on the same page.
// Parameters  : From : The html element to hide; To : The html element to show; Animation : To do a animation during the transition.(Default : YES)
// Return      : None
// #########################################

function switchUI(from,to,animations=true)
{
  if (animations == true) {

    var id = setInterval(frame, 50);
    i=1.0; // The opacity start at 1.0

    function frame() {
      i-=0.3;

      document.getElementById(from).style.opacity = i; //Hide the element to hide with a animation

      if (i <= 0)
      {
        document.getElementById(to).style.opacity = 1;
        document.getElementById(from).style.display = "none";
        document.getElementById(to).style.display = "block"; //show the contenent

        clearInterval(id); //Stop the function
      }
    }
  }
  else {
    document.getElementById(from).style.display = "none";
    document.getElementById(to).style.display = "block"; //show the contenent
  }
}
