const express = require('express');
const bodyParser = require('body-parser');
const app = express();

//For Bash execution
var sys = require('util')
var bash = require('child_process').exec;

//For Hash password and verify
const bcrypt = require('bcrypt');

//Count number of folders
const fs = require('fs');

var xml2js = require('xml2js');

app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.post('/login', (req, res) => {

  console.log(`User Id : ${req.body.userId} \nUser password: ${req.body.userPassword}.`);

  var passwordCheck = false;
  var userCheck = false;

  //Get the user stored in the setupVars
  user = bash("cat conf/setupVars.conf |grep USER |cut -d\"=\" -f2", function(err, userStored, stderr) {
    if (err) {
      console.log(`Get user -> an error occurred : ${err}`);
    }
    //Remove the \n
    userStored = userStored.split('\n')[0];
    if (req.body.userId == userStored)
    userCheck = true;
  });

  //Get the password store in the setupVars
  hash = bash("cat conf/setupVars.conf |grep WEBPASSWORD |cut -d\"=\" -f2", function(err, hashPassword, stderr) {
    if (err) {
      console.log(`Get hash password -> an error occurred : ${err}`);
    }
    //Remove the \n
    hashPassword = hashPassword.split('\n')[0];
    //Compare the input with the hash
    if (bcrypt.compareSync(req.body.userPassword, hashPassword))
    {
      console.log("Passwords matche");
      passwordCheck = true;

    }
    else
    console.log("Passwords don't matche\n\n");
    LoginCheck();
  });

  function LoginCheck()
  {
    console.log("passwordCheck :" + passwordCheck);
    console.log("userCheck :" + userCheck);
    if(passwordCheck == false || userCheck == false)
    {
      //Send the message to the web page
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      // req.on('data', function (chunk) {
      //   console.log('GOT DATA!');
      // });
      res.end('{\"msg\": \"Wrong user or password. Try again or click Forgot password to reset it.\"}');
    }
  }
  console.log("\n\n");
});




//Get the infos of apps
app.post('/getApps', (req, res) => {

  const dir = './apps';
  var jsonAppsInfo = "";
  fs.readdir(dir, (err, files) => {
    if (err) {
      throw err;
    }
    let totalFiles = files.length;
    var itemsProcessed = 0;
    files.forEach (appFolder => {
      // console.log("App folder : " + appFolder);
      //Get the user stored in the setupVars
      user = bash(`cat apps/${appFolder}/appinfo/info.json`, function(err, getJsonAppInfo, stderr) {
        if (err) {
          console.log(`Get user -> an error occurred : ${err}`);
        }
        //Remove the \n
        getJsonAppInfo = getJsonAppInfo.split('\n')[0];
        //Get appinfo Json like : {"name": "Test", "version": "0.0.1", "author": "firsname NAME", "license": "agpl"}

        jsonAppsInfo = `${jsonAppsInfo}${getJsonAppInfo},`;
        //Link différent Json app Info in the same string like : {"name": "Dashboard", "version": "0.0.1", "author": "Adrien DERACHE", "license": "agpl"},{"name": "Test", "version": "0.0.1", "author": "Adrien DERACHE", "license": "agpl"},

        itemsProcessed++;
        //If all is completed, call sendJSONAppsInfo
        if(itemsProcessed == totalFiles)
        sendJSONAppsInfo();

      });
    });
  });

  async function sendJSONAppsInfo() {
    //Generate the last Json Apps Info.
    jsonAppsInfo = `{"appsInfo":[${jsonAppsInfo}{\"name\":\"none\",\"version\": \"none\",\"author\": \"none\",\"license\": \"none\"}]}`;
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    console.log(jsonAppsInfo+"\n");
    res.end(`${jsonAppsInfo}`);
    // {"appsInfo":[{"name": "Dashboard", "version": "0.0.1", "author": "Adrien DERACHE", "license": "agpl"},{"name": "Test", "version": "0.0.1", "author": "Adrien DERACHE", "license": "agpl"},{"name":"none","version": "none","author": "none","license": "none"}]}
  }
});



//Get the user info
app.post('/userInfo', (req, res) => {

  var sendUserInfos = "";

  //Get the USER email stored in the setupVars
  hash = bash("cat conf/setupVars.conf |grep USEREMAIL |cut -d\"=\" -f2", function(err, userMail, stderr) {
    if (err) {
      console.log(`Get user email -> an error occurred : ${err}`);
    }
    //Remove the \n
    userMail = userMail.split('\n')[0];
    console.log(userMail);
    sendUserInfos = `\"userMail\": \"${userMail}\"`


    //Get the USER name stored in the setupVars
    bash("cat conf/setupVars.conf |grep USER |cut -d\"=\" -f2",  function(err, user, stderr) {
      if (err) {
        console.log(`Get user -> an error occurred : ${err}`);
      }
      //Remove the \n
      user = user.split('\n')[0];
      sendUserInfos = `${sendUserInfos},\"user\": \"${user}\"`
      console.log(user);
      sendJSON();
    });
  });

  function sendJSON()
  {
    console.log(sendUserInfos);
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end(`{${sendUserInfos}}`);
  }
});


//Get the infos of Web Site
//Return the name and its directory
app.post('/getWebSiteInfo', (req, res) => {
  //Get the name and directory
  console.log("test");
  bash("ls /etc/apache2/sites-available/ |sed 's/.conf//'",  function(err, webSiteInfo, stderr) {
    if (err) {
      console.log(`Get user -> an error occurred : ${err}`);
    }
    //Remove the \n
    webSiteInfo = webSiteInfo.split('\n')
    webSiteInfo = JSON.stringify(webSiteInfo);
    // sendUserInfos = `${sendUserInfos},\"user\": \"${user}\"`
    console.log(webSiteInfo);
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end(`${webSiteInfo}`);
  });
});


const port = 3000;

app.listen(port, () => {
  console.log(`Server running on port${port}`);
});
